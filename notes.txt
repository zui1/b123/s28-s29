What is an API?

	is an Application Programming Interface.
	This is the part of the server responsible for receivingreqests and sending responses. It is also the one responsible for handling your routes and the proper responses.

What is a REST API?

	is a Representational State Transfer.
	It is an architectiral style of communication between applications.

What problems does it solve?

	The need to separate user interface concerns of the client from the data storage concerns of the server.

	Enables a decoupled server which can understand, process and respond to client requests without knowing client state.

In REST, our requests has all of the information that server needs from our client.

	A REST API request:

		It is usually dictataed by an HTTP Method. Through an HTTP method, the action needed to be done is determined.

		A REST request contains additional information about the request: Headers.

		A body which contains data that needs to be sent to a server.

		We can operate and access resources through URL path

	In a REST API, all that a server needs from the client is already in the request and all a client needs from the server, is in the response.

	Postman

	It is a testing tool to test API endpoints and routes. It is a client with which we can create requests from and receive our server/api's responses.