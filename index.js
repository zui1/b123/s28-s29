//loading the express.js module into our app and into the express variable.
//express module will allow us to user express.js methods to create our api.
const express = require("express");

//Create an application with express.js
//This creates an express.js application and stores it as app.
//app is our server.
const app = express();

//port is a variable to contain the port when we want to designate.
const port = 4000;

//express.json() allows to handle the request's body and automatically parse the incoming JSON to a JS object we can access and manage.
app.use(express.json());

let users = [

    {
        email: "mighty12@gmail.com",
        username: "mightyMouse12",
        password: "notrelatedtomickey",
        isAdmin: false
    },
    {
        email: "minnieMouse@gmail.com",
        username: "minniexmickey",
        password: "minniesincethestart",
        isAdmin: false
    },
    {
        email: "mickeyTheMouse@gmail.com",
        username: "mickeyKing",
        password: "thefacethatrunstheplace",
        isAdmin: true
    }

];

let items = [];

let loggedUser;
//Express has methods to use as routes corresponding to each HTTP method.
//get(<endpoint>,<functionToHandle request and responses>)
app.get("/", (req, res) => {

    //Once the route is accessed, we can send a response with the use of res.send()
    //res.send() actually combines writeHead() abd end () already.
    //It is used to send a response to the client and is the end of the response.
    res.send("Hello World!");

})

app.get("/hello", (req, res) => {

    res.send("Hello from Batch 123!");

})

app.post("/", (req, res) => {

    //req.body will contain the body of a request
    console.log(req.body);

    res.send(`Hello, I'm ${req.body.name}. I am ${req.body.age} years old. I could be described as ${req.body.description}.`);

})

//register 
app.post("/users", (req, res) => {

    //since 2 applications are communicating with one another, being our client api, it is a good practice to always console log the incoming data first.
    console.log(req.body);

    //simulate creation of new user document
    let newUser = {

        email: req.body.email,
        username: req.body.username,
        password: req.body.password
    };

    users.push(newUser);
    console.log(users);
    res.send(`Registered Successfully.`)

})

//login

app.post("/users/login", (req, res) => {

    //should contain username and password
    console.log(req.body);

    //find the user with the same username and password from our request body.
    let foundUser = users.find((user) => {

        return user.username === req.body.username && user.password === req.body.password;

    });




    if (foundUser !== undefined) {

        //get the index number of the foundUser, but since the users array is an array of objects we have to use findIndex() instead, Will iterate over all of the items and return the index number of the current item that matches the return condition. It is similar to find() but instead returns only the index number.

        let foundUserIndex = users.findIndex((user) => {

            return user.username === foundUser.username

        });
        //This will add the index of your foundUser in the foundUser object.
        foundUser.index = foundUserIndex;
        //Temporarily log our user in. Allows us to refer the details of a logged in user.
        loggedUser = foundUser;
        console.log(loggedUser);

        res.send('Thank you for logging in.')

    } else {

        loggedUser = foundUser;
        res.send('Login failed. Wronf Credentials.')

    }

})

//addItem
app.post('/items', (req, res) => {

    console.log(loggedUser);
    console.log(req.body);

    if (loggedUser.isAdmin === true) {
        let newItem = {

            name: req.body.name,
            description: req.body.description,
            price: req.body.price,
            isActive: req.body.isActive

        };

        items.push(newItem);
        console.log(items);
        res.send("You have added a new item.")
    } else {

        res.send("Unauthorized: Action Forbidden.")

    }

})

//getAllItem
app.get('/allitems', (req, res) => {

    console.log(loggedUser);

    if (loggedUser.isAdmin === true) {

        res.send(items)

    } else {

        res.send("Unauthorized: Action Forbidden.")

    }

})

//getSingleUser
//GET request should not have a request body. It may have headers for additional information or we can add or small amount of data somewhere else: the url.
//Route params are values we can pass via the URL.
//This is done especially to allow us to send small amount of data into our server.
//Route parameters can be defined in the endpoint of a route with :parameterName
app.get('/users/:index', (req, res) => {
    //req.params is an object that contains the route params.
    //Its properties are then determined by your route parameters
    console.log(req.params);
    //how do we access the actual route params?
    console.log(req.params.index);
    //req.params.index being a part of the url string is a string, so we need to parse it as a proper int.
    let index = parseInt(req.params.index);
    /*console.log(typeof index);*/
    let user = users[index];
    res.send(user);

})

//updateUser
//We're going to update the password of our user. However we should get the user first. To do this we should not pass the index of the user in the body but instead in our route params.
app.put('/users/:index', (req, res) => {

    console.log(req.params);
    console.log(req.params.index);
    let userIndex = parseInt(req.params.index);
    if (loggedUser !== undefined && loggedUser.index === userIndex) {

        //get the proper user from the array with our index:
        //req.body.password comes from the body of your request.
        users[userIndex].password = req.body.password;
        console.log(users[userIndex]);
        res.send('User password has been updated');

    } else {

        res.send('Unauthorized. Login first.')

    }


})
/*ACTIVITY*/
app.get('/items/getSingle/:index', (req, res) => {

    console.log(req.params);
    console.log(req.params.index);
    let index = parseInt(req.params.index);
    let item = items[index];
    res.send(item)

});

app.put('/items/archive/:index', (req, res) => {

    console.log(req.params);
    console.log(req.params.index);
    let itemIndex = parseInt(req.params.index);
    console.log(loggedUser);
    if (loggedUser.isAdmin === true) {

        items[itemIndex].isActive = false;
        console.log(items[itemIndex]);
        res.send('Item Archived.')

    } else {

        res.send('Unauthorized: Action Forbidden')

    }

});

app.put('/items/activate/:index', (req, res) => {

    console.log(req.params);
    console.log(req.params.index);
    let itemIndex = parseInt(req.params.index);
    console.log(loggedUser);
    if (loggedUser.isAdmin === true) {

        items[itemIndex].isActive = true;
        console.log(items[itemIndex]);
        res.send('Item Activated.')

    } else {

        res.send('Unauthorized: Action Forbidden')

    }

});


app.listen(port, () => console.log(`Server is running at port ${port}`));